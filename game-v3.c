#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#define SIZE 3

// Инициализации игрового поля
void initializeBoard(char board[SIZE][SIZE]) {
    for (int i = 0; i < SIZE; ++i) {
        for (int j = 0; j < SIZE; ++j) {
            board[i][j] = ' ';
        }
    }
}


// Отображение игрового поля с числовыми обозначениями
void printBoard(char board[SIZE][SIZE]) {
    printf("\n  "); // Добавляем отступ перед шапкой с номерами столбцов
    for (int j = 0; j < SIZE; ++j) {
        printf("%d ", j + 1); // Печать номеров столбцов
    }
    printf("\n");

    for (int i = 0; i < SIZE; ++i) {
        printf("%d ", i + 1); // Печать номера строки перед каждой строкой
        for (int j = 0; j < SIZE; ++j) {
            printf("%c", board[i][j]);
            if (j < SIZE - 1) {
                printf("|"); // Вертикальные разделители
            }
        }
        printf("\n");
        if (i < SIZE - 1) {
            printf("  "); // Добавляем отступ перед горизонтальными разделителями
            for (int k = 0; k < SIZE - 1; ++k) {
                printf("--"); // Горизонтальные разделители
            }
            printf("-\n"); // Закрываем последний разделитель строки
        }
    }
}

// Установки хода
bool makeMove(char board[SIZE][SIZE], int row, int col, char player) {
    if (row >= 0 && row < SIZE && col >= 0 && col < SIZE && board[row][col] == ' ') {
        board[row][col] = player;
        return true;
    }
    return false;
}

// Проверки победителя
char checkWinner(char board[SIZE][SIZE]) {
    // Проверка строк и столбцов
    for (int i = 0; i < SIZE; ++i) {
        if (board[i][0] != ' ' && board[i][0] == board[i][1] && board[i][0] == board[i][2]) return board[i][0];
        if (board[0][i] != ' ' && board[0][i] == board[1][i] && board[0][i] == board[2][i]) return board[0][i];
    }

    // Проверка диагоналей
    if (board[0][0] != ' ' && board[0][0] == board[1][1] && board[0][0] == board[2][2]) return board[0][0];
    if (board[0][2] != ' ' && board[0][2] == board[1][1] && board[0][2] == board[2][0]) return board[0][2];

    // Пока победитель не определен
    return ' ';
}


int main() {
    char board[SIZE][SIZE];
    char playerNameX[100];
    char playerNameO[100];

    // Запрашиваем имена игроков
    printf("Введите имя игрока, который будет играть за крестики (X) : ");
    fgets(playerNameX, 100, stdin);
    playerNameX[strcspn(playerNameX, "\n")] = 0;

    printf("Введите имя игрока, который будет играть за нолики (O) : ");
    fgets(playerNameO, 100, stdin);
    playerNameO[strcspn(playerNameO, "\n")] = 0;

    initializeBoard(board);

    int moveCount = 0;
    int row, col;
    char player = 'X';  // X начинает игру
    char currentPlayerName[100];  // Имя текущего игрока
    char winner = ' ';

    while (moveCount < SIZE * SIZE && winner == ' ') {
        // Выбираем имя текущего игрока в зависимости от игрового символа
        if (player == 'X') {
            strncpy(currentPlayerName, playerNameX, sizeof(currentPlayerName) - 1);
            currentPlayerName[sizeof(currentPlayerName) - 1] = '\0'; 
        } else {
            strncpy(currentPlayerName, playerNameO, sizeof(currentPlayerName) - 1);
            currentPlayerName[sizeof(currentPlayerName) - 1] = '\0'; 
        }

        printBoard(board);
        printf("Игрок %s (%c), введите координаты хода (номер строки и номер столбца): ", currentPlayerName, player);
        scanf("%d %d", &row, &col);
        getchar(); // Поглощаем символ новой строки после ввода чисел
        row--; // Уменьшаем, так как массив начинается с 0
        col--;

        if (makeMove(board, row, col, player)) {
            winner = checkWinner(board);
            moveCount++;
            player = (player == 'X') ? 'O' : 'X'; // Смена игрока
        } else {
            printf("Эта клетка уже занята или введены неверные координаты! Попробуйте ещё раз.\n");
        }
    }

    printBoard(board);

    if (winner != ' ') {
        printf("Победил игрок %s!\n", (winner == 'X') ? playerNameX : playerNameO);
    } else {
        printf("Ничья!\n");
    }

    return 0;
}



